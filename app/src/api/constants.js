export const BASE_URL = process.env.NEXT_PUBLIC_CDN_BASE_URL;

export const CART_STORAGE_KEY = "cart";