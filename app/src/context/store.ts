import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'

import selectorReducer from '../features/selector/selectorSlice'
import cartReducer from '../features/cart/cartSlice'

export const store = configureStore({
  reducer: {
    selector: selectorReducer,
    cart: cartReducer
  },
  // middleware: (getDefaultMiddleware) => {
  //   return getDefaultMiddleware().concat(apiSlice.middleware);
  // }
})

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;