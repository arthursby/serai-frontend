/* /pages/index.js */
import Menu from "../features/menu/Menu";

const Home = () => {
  return <Menu title="Pizza Menu" src="pizza.json"/>
}
export default Home;