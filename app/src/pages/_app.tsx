import Layout from "../components/layout";
import '../styles/customTheme.scss'
import {store} from '../context/store';
import { Provider } from 'react-redux';

import type { AppProps } from 'next/app';

const PizzaApp = ({Component, pageProps}: AppProps) => {
    return (
        <Provider store={store}>
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </Provider>
    );}

export default PizzaApp
