import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState, AppThunk } from "../../context/store";
import {CART_STORAGE_KEY} from "../../api/constants";

const initialState = {
  items: [],
  total: 0,
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    persist: (state, action) => {
      const cart = action.payload;
      // should probably be done async with a thunk
      localStorage.setItem(CART_STORAGE_KEY, JSON.stringify(cart));
      state.total = cart.total;
      state.items = cart.items;
    },
    restore: (state, action) => {
      const cart = action.payload;
      console.info(`Restorinng cart `, cart);
      state.items = cart.items;
      state.total = cart.total;
    },
    checkout: (state) => {
      // should probably be done async with a thunk
      localStorage.removeItem(CART_STORAGE_KEY);
      state.items = initialState.items;
      state.total = initialState.total;
      alert("Congratulation, your order has been placed!");
    }
  }
});

// Action creators are generated for each case reducer function
export const { persist, restore, checkout } = cartSlice.actions;

export default cartSlice.reducer;
