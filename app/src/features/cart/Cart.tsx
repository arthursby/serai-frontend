import React from "react";
import { Button, Card, CardBody } from "reactstrap";
import { useAppSelector, useAppDispatch } from '../../context/hooks';
import { checkout, persist } from "./cartSlice";


export const Cart = () => {
  const dispatch = useAppDispatch();

  const cart = useAppSelector((state) => state.cart);
  const hasItems = cart?.items?.length > 0;

  const removeItem = (item) => {
    let { items, total } = cart;
    const index = items.findIndex((i) => i.id === item.id);

    // console.debug(`search for ${item.id} --> found=${index} `, items);
    let copy = [...items];
    copy.splice(0,1); // Remove found item
    
    const newCart = {
      items: [...copy],
      total: (total - item.price)
    }
    dispatch(persist(newCart));
  }

  return (
    <div className="sticky-top">
      <Card className="cart" style={{marginBottom: "20px"}}>
        <CardBody style={{ padding: 10 }}>
          {!hasItems && <div className="cart-notice">No items in your basket</div>}
          <div className="cart-content">
            {hasItems && cart.items.map((item, idx) => (
                <div key={`cart_${idx}_${item.id}`}>
                  <div className="item">
                    <div>
                      <button type="button" className="btn btn-close btn-secondary" aria-label="Close" onClick={() => removeItem(item)}></button>
                    </div>
                    <div className="item-content">
                      <div className="item-name">
                        <span className="item-quantity">{item.quantity}x</span> {item.name}
                      </div>
                    </div>
                    <span className="item-price">&nbsp; ${item.price * item.quantity}</span>
                  </div>
                </div>
            ))}
          </div>
          <hr/>
          <div className="cart-total">
            <span className="cart-total-title">Total</span><span>${cart?.total.toFixed(2)}</span>
          </div>
          <div className="mt-5">
            <Button  color="primary" style={{ width: "100%"}} disabled={!hasItems} onClick={() => dispatch(checkout())}>
                Choose
            </Button>
          </div>          
        </CardBody>
      </Card>
      <style jsx>{`
        .item:hover {
          border-color: 1px solid black;
        }        
        .cart-notice { text-align:center; margin-top:20px; margin-bottom: 20px;}
        .cart-total { text-align: right; font-weight: bold;} 
        .cart-total span { padding-right: 20px;}
        .item { display: flex; font-size: 1em;}
        .item-content { flex-grow:1; text-align: right; }
        .item-price {
          color: black;
        }
        .item-quantity {
          padding-bottom: 4px;
          padding-right: 5px;
          color: rgba(158, 158, 158, 1);
        }
        .item-name {
          color: rgba(97, 97, 97, 1);
        }
      `}</style>
    </div>
  );
};
