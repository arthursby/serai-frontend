import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  size: null,
  toppings: [],
  price: 0,
  selected: null
};

export const selectorSlice = createSlice({
  name: 'selector',
  initialState,
  reducers: {
    addTopping: (state, action) => {
      const toppings  = state.toppings;
      const topping = action.payload;
      state.toppings =  [...toppings, topping ];
    },
    removeTopping: (state,action) => {
      const topping = action.payload;
      const toppings  = state.toppings;
      const index = toppings.findIndex((t) => t.id === topping.id);
      // console.debug(`remove topping index=${index}`, topping, toppings);
      toppings.splice(index, 1);
      state.toppings = toppings;
    },
    setSize: (state,action) => {
        state.size = action.payload;
    },
    resetSelection: (state) => {
      state.size = initialState.size;
      state.toppings = initialState.toppings;
    },
    refreshSelectionPrice: (state) => {
      let price = 0;
      if(state.size) {
        price += state.size.price;
      }
      let selected = state.selected;
      let cartItem = {...selected}; // Make a copy of the selection
      // Replace by selected values
      cartItem.sizes = state.size;
      cartItem.toppings?.forEach( (topping) => {
          price += topping.price;
      })
      state.selectionPrice = price;
    },
    setSelected: (state, action) => {
      state.selected = action.payload;
    }
  }
})

// Action creators are generated for each case reducer function
export const { addTopping, removeTopping, setSize, refreshSelectionPrice, resetSelection, setSelected } = selectorSlice.actions

export default selectorSlice.reducer