import { useContext, useEffect, useState } from "react";
import { useAppSelector, useAppDispatch } from '../../context/hooks';
import { setSize } from "./selectorSlice";

import { Input, Col, Row, Label } from "reactstrap";

export const SizeSelector = ({dish}) => { 
    const {sizes} = dish;
    const dispatch = useAppDispatch()

    return <Row className="mt-4">
            {sizes.map( (size, idx) => 
            <Col  key={`sel_${dish.id}_${size.id}_${idx}`}  xs={4}>
                <Label>
                    <Input type="radio" name={`dish_${dish.id}`} onChange={(e) => dispatch(setSize(size))}/> {size.name} 
                </Label>
            </Col> 
            )}
        </Row> 
}

