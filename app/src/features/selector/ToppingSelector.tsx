import { useContext, useEffect, useState } from "react";
import { useAppSelector, useAppDispatch } from '../../context/hooks';
import { addTopping, removeTopping } from "./selectorSlice";

import { Input, Col, Row, Label } from "reactstrap";

export const ToppingSelector = ({ dish }) => {
  const { toppings } = dish;
  const dispatch = useAppDispatch()

  const onChange = ({ topping, checked }) => {
    // console.debug(`changing topping checked=${checked}`, topping );
    if (checked) {
      dispatch(addTopping(topping));
    } else {
      dispatch(removeTopping(topping));
    }
  };

  return (
    <Row>
      {toppings.map((t, idx) => (
        <Col key={`sel_${dish.id}_${t.id}_${idx}`} xs={4}>
          <Label>
            <Input
              type="checkbox"
              onChange={(e) =>
                onChange({ topping: t, checked: e.target.checked })
              }
            />{" "}
            {t.name}
          </Label>
        </Col>
      ))}
    </Row>
  );
};
