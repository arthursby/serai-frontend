import type { NextPage } from 'next'

import { useEffect, useState } from "react";
import Head from 'next/head'
import { Modal, ModalBody,  ModalFooter, Input, Label } from "reactstrap";
import { useAppSelector, useAppDispatch } from '../../context/hooks';

import axios from "axios";
import {
    Button,
    Card,
    CardBody,
    CardImg,
    CardTitle,
    Col,
    Row,
} from "reactstrap";

import { addTopping, removeTopping, setSize, setSelected, resetSelection } from "../selector/selectorSlice";
import { restore, persist } from "../cart/cartSlice";
import {CART_STORAGE_KEY} from "../../api/constants";

import { Cart } from "../cart/Cart";
import { ToppingSelector } from "../selector/ToppingSelector";
import { SizeSelector } from "../selector/SizeSelector";

const Menu = ({title, src}) => {
    const dispatch = useAppDispatch();

    const [data,setData] = useState<any>();
    const cart = useAppSelector((state) => state.cart);
    const selector = useAppSelector((state) => state.selector);
    const hasSelection = selector.selected != null;
    const hasSizeSelection = selector.size!=null;
    const {selected} = selector;

    useEffect(  () => {
        async function fetchData() {
            console.info(`load data from ${src}`);
            const response = await axios.get(src);
            setData(response.data);
        }
        fetchData();        
    }, []) // Load data only once

    useEffect( () => {
        // Restore cart from cookie, this could also be tracked in a db or localstorage.
        let cart = localStorage.getItem(CART_STORAGE_KEY);

        if (typeof cart === "string" && cart !== "undefined") {
            cart = JSON.parse(cart);
            dispatch(restore(cart));
        }
    }, []) // Load once on startup

    const addToCart = () => {
        // Compute price and add to cart.
        let price = selector.size.price;
        // Make a copy of the selected dish to be overwritted with chosen toppings / size
        let cartItem = {...selector.selected}; 
        cartItem.sizes = selector.size;
        cartItem.toppings = selector.toppings;
        // Compute selection price
        cartItem.toppings.forEach( (topping) => {
            price += topping.price;
        })
        cartItem.price = price;
        cartItem.quantity = 1;
        
        // Should be replaced by thunk and then.
        const newCart = {...cart};
        const items = newCart.items;
        newCart.total += price;
        newCart.items = [...items, cartItem];
        dispatch(persist(newCart));
        dispatch(setSelected(null));
    }

    if (data && data.dishes) {
        const { dishes } = data;
        return (
            <div style={{paddingTop: 20, maxWidth: "1300px", margin: "auto"}}>
                <Head>
                    <title>{title}</title>
                </Head>
                <Row className="g-0">
                    <Col sm="8" md="9" lg="8" xl="8" className="order-2 order-sm-1">
                        <Row className="g-0">
                            {dishes?.map((dish, idx) => 
                                <Col lg="4" md="6" sm="6" key={`key_${dish.id}_${idx}`} style={{marginBottom:10, paddingRight: 10}}>                            
                                    <Card>
                                        <CardImg
                                            top={true}
                                            style={{ height: 150, padding: 10, borderRadius:"20px" }}
                                            src={`${process.env.NEXT_PUBLIC_CDN_BASE_URL}${dish.image}`}
                                        />
                                        <CardBody>
                                            <CardTitle>{dish.name} - ${dish.basePrice}</CardTitle>
                                        </CardBody>
                                        <div className="card-footer">
                                            <Button  outline color="primary" style={{ width: "100%"}} onClick={() => dispatch(setSelected(dish))}>
                                                Choose
                                            </Button>
                                        </div>
                                    </Card>
                                </Col>
                            )
                            }
                        </Row>
                    </Col>
                    <Col sm="4" md="3" lg="4" xl="3" className="order-1 order-sm-2">
                        <Cart />
                    </Col>
                </Row>
                <Modal isOpen={hasSelection} onClosed={ () => dispatch(resetSelection()) } className="modal-xl ">
                    {hasSelection && 
                    <ModalBody>
                        <div className="container">
                        <ToppingSelector dish={selected}></ToppingSelector>
                        <SizeSelector dish={selected}></SizeSelector>
                        </div>
                    </ModalBody>
                    }
                    <ModalFooter>
                    <div>
                        <Button onClick={ addToCart } disabled={!hasSizeSelection}
                            color="primary" className="action" style={{width: 150}}
                            >
                            Add to Basket
                        </Button>
                    </div>
                    <div className="selection-center"></div>
                    <div>
                        <Button onClick={ () => dispatch(setSelected(null))} style={{width: 150}}>
                            Cancel
                        </Button>
                    </div>
                    </ModalFooter>
                </Modal>
                <style jsx>
                    {`
                    .modal-footer { display:flex; }
                    .selection-center { flex-grow:1; }
                    
                    .card { margin-bottom: 10px;}
                    .btn-outline-primary {
                        color: #007bff !important;
                    }
                    button.btn { width:100%; }
                    a:hover {
                        color: white !important;
                    }
                    .card-footer { 
                        background-color: transparent; 
                        border-top: none;
                    }
                    .card-img-top { 
                        padding: 10px;
                    }
                    `}
                </style>
            </div>
        );
    }
    return <>No data</>
}

export default Menu;