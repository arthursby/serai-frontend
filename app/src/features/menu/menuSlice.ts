import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  dishes: []
};

export const menuSlice = createSlice({
  name: 'menu',
  initialState,
  reducers: {
  }
})

// Action creators are generated for each case reducer function
// export const {  } = menuSlice.actions

export default menuSlice.reducer