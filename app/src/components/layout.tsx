import React, { useEffect } from "react";
import Head from "next/head";

const Layout = ({children}) => {
  return (
    <div>
      <Head>
        <title>SERAI Pizza Shop</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      {children}
    </div>
  );
};

export default Layout;