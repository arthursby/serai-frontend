## Skaffold kubernetes dev

```bash
skaffold dev 
```

## Local dev
```bash
cd app
yarn install
yarn run dev
```

## Redux

The redux store is only used on the client but it possible to use a wrapper if server side rendering is required so the store can be called within getInitialProps.

https://github.com/kirill-konshin/next-redux-wrapper#usage-with-redux-saga

## Styling 
- Layout based on bootstrap extended with SASS
- Components are also styled locally leveraging nextjs <style jsx> capabilty
